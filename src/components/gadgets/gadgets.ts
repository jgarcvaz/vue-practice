export default {
  name: "gadgets",
  props: ['hero'],
  components: {
  },
  data() {
    return {};
  },
  methods: {
    addGadget(hero, gadget) {
      hero.gadgets.push(gadget);
      hero.newGadget = '';
    }
  }
};