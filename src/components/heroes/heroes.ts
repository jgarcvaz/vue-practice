// @ts-ignore
import gadgets from "../gadgets/index.vue";

export default {
  name: 'heroes',
  components: {
    gadgets
  },
  props: ['heroes'],
  data() {
    return {
      newHero: ''
    }
  },
  computed: {

  },
  mounted() {

  },
  methods: {
    addHero() {
      this.heroes.push({ name: this.newHero, gadgets: [] });
      this.newHero = '';
    }
  },
  directives: {
    focus: {
      // Definición de directiva
      inserted: function (el) {
        el.focus()
      }
    }
  }
}
