# Taller de VUE

1.Crear un componente heroes en el cual se me permita añadir un heroe.

2.Hacer un segundo componente llamado gadgets, el cual se comunicará con el componente heroes. Gadgets tendrá un formulario para añadir "gadgets" y se mostraran en heroes.

3.Crear una directiva que haga foco en el input de añadir heroe.

4.Crear un filter custom que ponga en mayusculas los nombres de los heroes.